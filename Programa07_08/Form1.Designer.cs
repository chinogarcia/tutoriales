﻿namespace Programa07_08
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNodo = new System.Windows.Forms.TextBox();
            this.txtElemento = new System.Windows.Forms.TextBox();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.btnElemento = new System.Windows.Forms.Button();
            this.btnLimpiarArbol = new System.Windows.Forms.Button();
            this.btnLimpiarNodo = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // txtNodo
            // 
            this.txtNodo.Location = new System.Drawing.Point(180, 12);
            this.txtNodo.Name = "txtNodo";
            this.txtNodo.Size = new System.Drawing.Size(100, 20);
            this.txtNodo.TabIndex = 0;
            // 
            // txtElemento
            // 
            this.txtElemento.Location = new System.Drawing.Point(180, 44);
            this.txtElemento.Name = "txtElemento";
            this.txtElemento.Size = new System.Drawing.Size(100, 20);
            this.txtElemento.TabIndex = 1;
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.Location = new System.Drawing.Point(310, 12);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(75, 23);
            this.btnAdicionar.TabIndex = 2;
            this.btnAdicionar.Text = "Adicionar Nodo";
            this.btnAdicionar.UseVisualStyleBackColor = true;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // btnElemento
            // 
            this.btnElemento.Location = new System.Drawing.Point(310, 41);
            this.btnElemento.Name = "btnElemento";
            this.btnElemento.Size = new System.Drawing.Size(75, 23);
            this.btnElemento.TabIndex = 3;
            this.btnElemento.Text = "Elemento";
            this.btnElemento.UseVisualStyleBackColor = true;
            this.btnElemento.Click += new System.EventHandler(this.btnElemento_Click);
            // 
            // btnLimpiarArbol
            // 
            this.btnLimpiarArbol.Location = new System.Drawing.Point(310, 89);
            this.btnLimpiarArbol.Name = "btnLimpiarArbol";
            this.btnLimpiarArbol.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiarArbol.TabIndex = 4;
            this.btnLimpiarArbol.Text = "Limpiar Arbol";
            this.btnLimpiarArbol.UseVisualStyleBackColor = true;
            this.btnLimpiarArbol.Click += new System.EventHandler(this.btnLimpiarArbol_Click);
            // 
            // btnLimpiarNodo
            // 
            this.btnLimpiarNodo.Location = new System.Drawing.Point(310, 118);
            this.btnLimpiarNodo.Name = "btnLimpiarNodo";
            this.btnLimpiarNodo.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiarNodo.TabIndex = 5;
            this.btnLimpiarNodo.Text = "Limipar nodo";
            this.btnLimpiarNodo.UseVisualStyleBackColor = true;
            this.btnLimpiarNodo.Click += new System.EventHandler(this.btnLimpiarNodo_Click);
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(13, 12);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(144, 202);
            this.treeView1.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 261);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.btnLimpiarNodo);
            this.Controls.Add(this.btnLimpiarArbol);
            this.Controls.Add(this.btnElemento);
            this.Controls.Add(this.btnAdicionar);
            this.Controls.Add(this.txtElemento);
            this.Controls.Add(this.txtNodo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNodo;
        private System.Windows.Forms.TextBox txtElemento;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.Button btnElemento;
        private System.Windows.Forms.Button btnLimpiarArbol;
        private System.Windows.Forms.Button btnLimpiarNodo;
        private System.Windows.Forms.TreeView treeView1;
    }
}

