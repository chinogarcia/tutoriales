﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Programa08_02.Properties;

namespace Programa08_02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtMensaje.Text = (string)Settings.Default["Mensaje"];

            chkImportado.Checked = (bool)Settings.Default["Importado"];
            chkOrganico.Checked = (bool)Settings.Default["Organico"];

            this.Size = (Size)Settings.Default["Tamano"];

            switch((int)Settings.Default["Frutas"])
            {
                case 0:
                    rdbManzana.Checked = true;
                    rbdBanana.Checked = false;
                    rbdCiruela.Checked = false;
                    break;
                case 1:
                    rdbManzana.Checked = false;
                    rbdBanana.Checked = true;
                    rbdCiruela.Checked = false;
                    break;
                case 2:
                    rdbManzana.Checked = false;
                    rbdBanana.Checked = false;
                    rbdCiruela.Checked = true;
                    break;
            }
        }

        private void txtMensaje_TextChanged(object sender, EventArgs e)
        {
            Settings.Default["Mensaje"] = txtMensaje.Text;
            Settings.Default.Save();
        }

        private void chkImportado_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default["Importado"] = chkImportado.Checked;
            Settings.Default.Save();
        }

        private void chkOrganico_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default["Organico"] = chkOrganico.Checked;
            Settings.Default.Save();
        }

        private void rdbManzana_CheckedChanged(object sender, EventArgs e)
        {
            if(rdbManzana.Checked)
            {
                Settings.Default["Frutas"] = 0;
                Settings.Default.Save();
            }
        }

        private void rbdBanana_CheckedChanged(object sender, EventArgs e)
        {
            if(rbdBanana.Checked)
            {
                Settings.Default["Frutas"] = 1;
                Settings.Default.Save();
            }
        }

        private void rbdCiruela_CheckedChanged(object sender, EventArgs e)
        {
            if (rbdCiruela.Checked)
            {
                Settings.Default["Frutas"] = 2;
                Settings.Default.Save();
            }
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            Settings.Default["Tamano"] = this.Size;
            Settings.Default.Save();
        }
    }
}
