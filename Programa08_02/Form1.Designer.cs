﻿namespace Programa08_02
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbManzana = new System.Windows.Forms.RadioButton();
            this.rbdBanana = new System.Windows.Forms.RadioButton();
            this.rbdCiruela = new System.Windows.Forms.RadioButton();
            this.chkImportado = new System.Windows.Forms.CheckBox();
            this.chkOrganico = new System.Windows.Forms.CheckBox();
            this.txtMensaje = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbdCiruela);
            this.groupBox1.Controls.Add(this.rbdBanana);
            this.groupBox1.Controls.Add(this.rdbManzana);
            this.groupBox1.Location = new System.Drawing.Point(12, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(130, 89);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // rdbManzana
            // 
            this.rdbManzana.AutoSize = true;
            this.rdbManzana.Checked = true;
            this.rdbManzana.Location = new System.Drawing.Point(16, 19);
            this.rdbManzana.Name = "rdbManzana";
            this.rdbManzana.Size = new System.Drawing.Size(69, 17);
            this.rdbManzana.TabIndex = 0;
            this.rdbManzana.TabStop = true;
            this.rdbManzana.Text = "Manzana";
            this.rdbManzana.UseVisualStyleBackColor = true;
            this.rdbManzana.CheckedChanged += new System.EventHandler(this.rdbManzana_CheckedChanged);
            // 
            // rbdBanana
            // 
            this.rbdBanana.AutoSize = true;
            this.rbdBanana.Location = new System.Drawing.Point(16, 42);
            this.rbdBanana.Name = "rbdBanana";
            this.rbdBanana.Size = new System.Drawing.Size(62, 17);
            this.rbdBanana.TabIndex = 1;
            this.rbdBanana.Text = "Banana";
            this.rbdBanana.UseVisualStyleBackColor = true;
            this.rbdBanana.CheckedChanged += new System.EventHandler(this.rbdBanana_CheckedChanged);
            // 
            // rbdCiruela
            // 
            this.rbdCiruela.AutoSize = true;
            this.rbdCiruela.Location = new System.Drawing.Point(16, 65);
            this.rbdCiruela.Name = "rbdCiruela";
            this.rbdCiruela.Size = new System.Drawing.Size(57, 17);
            this.rbdCiruela.TabIndex = 2;
            this.rbdCiruela.Text = "Ciruela";
            this.rbdCiruela.UseVisualStyleBackColor = true;
            this.rbdCiruela.CheckedChanged += new System.EventHandler(this.rbdCiruela_CheckedChanged);
            // 
            // chkImportado
            // 
            this.chkImportado.AutoSize = true;
            this.chkImportado.Location = new System.Drawing.Point(175, 70);
            this.chkImportado.Name = "chkImportado";
            this.chkImportado.Size = new System.Drawing.Size(73, 17);
            this.chkImportado.TabIndex = 1;
            this.chkImportado.Text = "Importado";
            this.chkImportado.UseVisualStyleBackColor = true;
            this.chkImportado.CheckedChanged += new System.EventHandler(this.chkImportado_CheckedChanged);
            // 
            // chkOrganico
            // 
            this.chkOrganico.AutoSize = true;
            this.chkOrganico.Location = new System.Drawing.Point(175, 93);
            this.chkOrganico.Name = "chkOrganico";
            this.chkOrganico.Size = new System.Drawing.Size(69, 17);
            this.chkOrganico.TabIndex = 2;
            this.chkOrganico.Text = "Organico";
            this.chkOrganico.UseVisualStyleBackColor = true;
            this.chkOrganico.CheckedChanged += new System.EventHandler(this.chkOrganico_CheckedChanged);
            // 
            // txtMensaje
            // 
            this.txtMensaje.Location = new System.Drawing.Point(13, 13);
            this.txtMensaje.Name = "txtMensaje";
            this.txtMensaje.Size = new System.Drawing.Size(198, 20);
            this.txtMensaje.TabIndex = 3;
            this.txtMensaje.TextChanged += new System.EventHandler(this.txtMensaje_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 171);
            this.Controls.Add(this.txtMensaje);
            this.Controls.Add(this.chkOrganico);
            this.Controls.Add(this.chkImportado);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResizeEnd += new System.EventHandler(this.Form1_ResizeEnd);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbdCiruela;
        private System.Windows.Forms.RadioButton rbdBanana;
        private System.Windows.Forms.RadioButton rdbManzana;
        private System.Windows.Forms.CheckBox chkImportado;
        private System.Windows.Forms.CheckBox chkOrganico;
        private System.Windows.Forms.TextBox txtMensaje;
    }
}

