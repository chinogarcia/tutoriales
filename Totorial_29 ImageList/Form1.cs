﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Totorial_29_ImageList
{
    public partial class Form1 : Form
    {
        private int indice;

        public Form1()
        {
            InitializeComponent();
            indice = 0; 
        }

        private void btnCambio_Click(object sender, EventArgs e)
        {
            indice++;

            if (indice > 14)
            { 
                indice = 0;
            }

            lblFoto.ImageIndex = indice;

            pcbImage.Image = imgL.Images[indice];
        }

        private void pcbImage_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pcbImage.Image = imgL.Images[0];
        }
    }
}
