﻿namespace Totorial_29_ImageList
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.imgL = new System.Windows.Forms.ImageList(this.components);
            this.lblFoto = new System.Windows.Forms.Label();
            this.btnCambio = new System.Windows.Forms.Button();
            this.pcbImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pcbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // imgL
            // 
            this.imgL.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgL.ImageStream")));
            this.imgL.TransparentColor = System.Drawing.Color.Transparent;
            this.imgL.Images.SetKeyName(0, "is0.png");
            this.imgL.Images.SetKeyName(1, "is1.png");
            this.imgL.Images.SetKeyName(2, "is2.png");
            this.imgL.Images.SetKeyName(3, "is3.png");
            this.imgL.Images.SetKeyName(4, "is4.png");
            this.imgL.Images.SetKeyName(5, "is5.png");
            this.imgL.Images.SetKeyName(6, "is6.png");
            this.imgL.Images.SetKeyName(7, "is7.png");
            this.imgL.Images.SetKeyName(8, "is8.png");
            this.imgL.Images.SetKeyName(9, "is9.png");
            this.imgL.Images.SetKeyName(10, "is10.png");
            this.imgL.Images.SetKeyName(11, "is11.png");
            this.imgL.Images.SetKeyName(12, "is12.png");
            this.imgL.Images.SetKeyName(13, "is13.png");
            this.imgL.Images.SetKeyName(14, "Bitmap1.bmp");
            // 
            // lblFoto
            // 
            this.lblFoto.ImageIndex = 0;
            this.lblFoto.ImageList = this.imgL;
            this.lblFoto.Location = new System.Drawing.Point(29, 9);
            this.lblFoto.Name = "lblFoto";
            this.lblFoto.Size = new System.Drawing.Size(100, 140);
            this.lblFoto.TabIndex = 0;
            this.lblFoto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCambio
            // 
            this.btnCambio.Location = new System.Drawing.Point(32, 175);
            this.btnCambio.Name = "btnCambio";
            this.btnCambio.Size = new System.Drawing.Size(105, 23);
            this.btnCambio.TabIndex = 1;
            this.btnCambio.Text = "Cambiar Imagen";
            this.btnCambio.UseVisualStyleBackColor = true;
            this.btnCambio.Click += new System.EventHandler(this.btnCambio_Click);
            // 
            // pcbImage
            // 
            this.pcbImage.Location = new System.Drawing.Point(159, 12);
            this.pcbImage.Name = "pcbImage";
            this.pcbImage.Size = new System.Drawing.Size(100, 140);
            this.pcbImage.TabIndex = 2;
            this.pcbImage.TabStop = false;
            this.pcbImage.Click += new System.EventHandler(this.pcbImage_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 261);
            this.Controls.Add(this.pcbImage);
            this.Controls.Add(this.btnCambio);
            this.Controls.Add(this.lblFoto);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pcbImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imgL;
        private System.Windows.Forms.Label lblFoto;
        private System.Windows.Forms.Button btnCambio;
        private System.Windows.Forms.PictureBox pcbImage;
    }
}

