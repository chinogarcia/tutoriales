﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programa05_04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dnubFrutas.Items.Add("Melon");
            dnubFrutas.Items.Add("Manzana");
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            dnubFrutas.Items.Add(txtFruta.Text);
            txtFruta.Text = "";
        }

        private void dnubFrutas_SelectedItemChanged(object sender, EventArgs e)
        {

            string fruta = (string)dnubFrutas.SelectedItem;

            lblMensaje.Text = string.Format("Tu Fruta faborita es {0},es muy sabrosa", fruta);
            
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            int indice = dnubFrutas.SelectedIndex;
            dnubFrutas.Items.RemoveAt(indice);
            dnubFrutas.SelectedIndex = 0;
        }
    }
}
