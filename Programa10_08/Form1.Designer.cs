﻿namespace Programa10_08
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.ncControl1 = new Programa10_08.NCControl();
            this.miBoton1 = new Programa10_08.MiBoton();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(33, 46);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "noControl1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // ncControl1
            // 
            this.ncControl1.Location = new System.Drawing.Point(33, 46);
            this.ncControl1.Name = "ncControl1";
            this.ncControl1.Size = new System.Drawing.Size(75, 23);
            this.ncControl1.TabIndex = 1;
            this.ncControl1.Text = "ncControl1";
            this.ncControl1.UseVisualStyleBackColor = true;
            // 
            // miBoton1
            // 
            this.miBoton1.Activado = false;
            this.miBoton1.Location = new System.Drawing.Point(33, 198);
            this.miBoton1.Name = "miBoton1";
            this.miBoton1.Size = new System.Drawing.Size(157, 41);
            this.miBoton1.TabIndex = 2;
            this.miBoton1.Text = "Dale click";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.miBoton1);
            this.Controls.Add(this.ncControl1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private NCControl ncControl1;
        private MiBoton miBoton1;
    }
}

