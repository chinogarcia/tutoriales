﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Totorial_31_32_ListView
{
    public partial class Form1 : Form
    {
        private ListViewGroup frutas = new ListViewGroup("Frutas", HorizontalAlignment.Left);
        private ListViewGroup carnes = new ListViewGroup("Carnes", HorizontalAlignment.Left);

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //ImageList
            ImageList misImagenes = new ImageList();
            misImagenes.ImageSize = new Size(100, 100);
            //obtner listado de imagenes 
            string[] archivos = Directory.GetFiles("png");
            //Cargar imagenes 
            try 
            {
                foreach (string archivo in archivos)
                    misImagenes.Images.Add(Image.FromFile(archivo));
            }
            catch 
            {
                MessageBox.Show("Los archivos se cargaron de forma erronea.");
            }
            lstvAlimentos.SmallImageList = misImagenes;

            lstvAlimentos.Items.Add(new ListViewItem("Manzana",0,frutas));
            lstvAlimentos.Items.Add(new ListViewItem("Banana",1, frutas));
            lstvAlimentos.Items.Add(new ListViewItem("Sandia",2, frutas));
            lstvAlimentos.Items.Add(new ListViewItem("Durazno",3, frutas));
            lstvAlimentos.Items.Add(new ListViewItem("Ciruela",4, frutas));

            ListViewItem elemento = new ListViewItem("Pollo",5,carnes);
            lstvAlimentos.Items.Add(elemento);

            lstvAlimentos.Items.Add(new ListViewItem("Res",6, carnes));
            lstvAlimentos.Items.Add(new ListViewItem("Pescado",7, carnes));
            lstvAlimentos.Items.Add(new ListViewItem("Cerdo",8, carnes));
            lstvAlimentos.Items.Add(new ListViewItem("Cocodrilo",9, carnes));

            lstvAlimentos.Groups.Add(frutas);
            lstvAlimentos.Groups.Add(carnes);


        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            if(rbtFrutas.Checked)
            {
                lstvAlimentos.Items.Add(new ListViewItem(txtElemento.Text, frutas));
            }

            if(rbtCarnes.Checked)
            {
                lstvAlimentos.Items.Add(new ListViewItem(txtElemento.Text, carnes));
            }
        }

        private void lstvAlimentos_MouseClick(object sender, MouseEventArgs e)
        {
            lblSeleccionado.Text = lstvAlimentos.SelectedItems[0].Text;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            lstvAlimentos.Items.Clear();
            lblSeleccionado.Text = "Seleccionado";
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            lstvAlimentos.SelectedItems[0].SubItems[0].Text = txtElemento.Text;
            lstvAlimentos.SelectedItems[0].ImageIndex = Convert.ToInt32(txtImagen.Text);
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            lstvAlimentos.Items.RemoveAt(lstvAlimentos.SelectedIndices[0]);
        }
    }
}
