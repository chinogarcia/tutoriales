﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programa10_03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void textBox1_DragDrop(object sender, DragEventArgs e)
        {
            txtContenidos.Text = "";
            // Obtendremos el arreglo con los archivos
            string[] archivos = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            string linea = "";

            lblArchivo.Text = archivos[0];

            // Leemos el archivo
            StreamReader lector = FileDialog.OpenText(archivos[0]);

            while ((linea = lector.ReadLine()) != null)
            {
                txtContenidos.Text += linea + "\r\n";
            }

            lector.Close();


        }

    }
}
