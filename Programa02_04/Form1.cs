﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programa02_04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void txtCotisacion_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCotisar_Click(object sender, EventArgs e)
        {
            double costo = 0.0;
            string cotizacion = "";

            cotizacion = "Cotizacion de auto para " + txtNombre.Text+"     ";
            //Obtenemos costo inicial
            costo = Convert.ToDouble(txtCosto.Text);
            //Verificar seguros
            if(rbnBasico.Checked==true)
            {
                costo = costo + 500.0;
                cotizacion += "lleva seguro basico de $500     ";

            }
            if(rbnTerceros.Checked==true)
            {
                costo += 700.0;
                cotizacion+= "lleva seguro a terceros de $700     ";

            }
            if (rbnTotal.Checked == true)
            {
                costo += 1000.0;
                cotizacion += "lleva seguro total de $1000   ";
            }

            //Verificar equipo
            if(chkAire.Checked==true)
            {
                costo += 500.0;
                cotizacion += "con aire acondicionado de $500    ";
            }
            if (chkAudio.Checked == true)
            {
                costo += 700.0;
                cotizacion += "con sistema de audio acondicionado de $700   ";
            }
            //mostramos total
            cotizacion += "el total a pagar es de" + costo.ToString();
            txtCotisacion.Text = cotizacion;
        }
    }
}
