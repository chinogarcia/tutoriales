﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programa09_01
{
    public partial class Form1 : Form
    {
        private int x = 0;
        private int y = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnDibuja_Click(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromHwnd(this.Handle);

            Font fuente = new Font("Futura Md BT", 35);

            g.DrawString("En metodo ", fuente , Brushes.IndianRed, 50, 75);

            g.Dispose();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Font fuente = new Font("Futura Md BT", 20);

            g.DrawString("Que tranza Banda como estan!!!", fuente, Brushes.DarkRed, x, y);
        }

        private void btnInvalidate_Click(object sender, EventArgs e)
        {
            x += 5;
            y += 17;

            Invalidate();
        }
    }
}
