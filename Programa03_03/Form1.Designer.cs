﻿namespace Programa03_03
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmrPrueba = new System.Windows.Forms.Timer(this.components);
            this.lblValor = new System.Windows.Forms.Label();
            this.btnInicio = new System.Windows.Forms.Button();
            this.btnDetener = new System.Windows.Forms.Button();
            this.tbrIntervalo = new System.Windows.Forms.TrackBar();
            this.lblTvalor = new System.Windows.Forms.Label();
            this.pdrTrabajo = new System.Windows.Forms.ProgressBar();
            this.btnCalculo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tbrIntervalo)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrPrueba
            // 
            this.tmrPrueba.Interval = 500;
            this.tmrPrueba.Tick += new System.EventHandler(this.tmrPrueba_Tick);
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Location = new System.Drawing.Point(31, 24);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(35, 13);
            this.lblValor.TabIndex = 0;
            this.lblValor.Text = "label1";
            // 
            // btnInicio
            // 
            this.btnInicio.Location = new System.Drawing.Point(194, 24);
            this.btnInicio.Name = "btnInicio";
            this.btnInicio.Size = new System.Drawing.Size(75, 23);
            this.btnInicio.TabIndex = 1;
            this.btnInicio.Text = "Inicio";
            this.btnInicio.UseVisualStyleBackColor = true;
            this.btnInicio.Click += new System.EventHandler(this.btnInicio_Click);
            // 
            // btnDetener
            // 
            this.btnDetener.Location = new System.Drawing.Point(194, 75);
            this.btnDetener.Name = "btnDetener";
            this.btnDetener.Size = new System.Drawing.Size(75, 23);
            this.btnDetener.TabIndex = 2;
            this.btnDetener.Text = "Detener";
            this.btnDetener.UseVisualStyleBackColor = true;
            this.btnDetener.Click += new System.EventHandler(this.btnDetener_Click);
            // 
            // tbrIntervalo
            // 
            this.tbrIntervalo.Location = new System.Drawing.Point(12, 122);
            this.tbrIntervalo.Maximum = 1000;
            this.tbrIntervalo.Minimum = 100;
            this.tbrIntervalo.Name = "tbrIntervalo";
            this.tbrIntervalo.Size = new System.Drawing.Size(260, 45);
            this.tbrIntervalo.TabIndex = 3;
            this.tbrIntervalo.TickFrequency = 100;
            this.tbrIntervalo.Value = 100;
            this.tbrIntervalo.Scroll += new System.EventHandler(this.tbrIntervalo_Scroll);
            // 
            // lblTvalor
            // 
            this.lblTvalor.AutoSize = true;
            this.lblTvalor.Location = new System.Drawing.Point(31, 75);
            this.lblTvalor.Name = "lblTvalor";
            this.lblTvalor.Size = new System.Drawing.Size(35, 13);
            this.lblTvalor.TabIndex = 4;
            this.lblTvalor.Text = "label1";
            // 
            // pdrTrabajo
            // 
            this.pdrTrabajo.Location = new System.Drawing.Point(12, 174);
            this.pdrTrabajo.Name = "pdrTrabajo";
            this.pdrTrabajo.Size = new System.Drawing.Size(257, 23);
            this.pdrTrabajo.TabIndex = 5;
            // 
            // btnCalculo
            // 
            this.btnCalculo.Location = new System.Drawing.Point(194, 223);
            this.btnCalculo.Name = "btnCalculo";
            this.btnCalculo.Size = new System.Drawing.Size(75, 23);
            this.btnCalculo.TabIndex = 6;
            this.btnCalculo.Text = "Calcular";
            this.btnCalculo.UseVisualStyleBackColor = true;
            this.btnCalculo.Click += new System.EventHandler(this.btnCalculo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnCalculo);
            this.Controls.Add(this.pdrTrabajo);
            this.Controls.Add(this.lblTvalor);
            this.Controls.Add(this.tbrIntervalo);
            this.Controls.Add(this.btnDetener);
            this.Controls.Add(this.btnInicio);
            this.Controls.Add(this.lblValor);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.tbrIntervalo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmrPrueba;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.Button btnInicio;
        private System.Windows.Forms.Button btnDetener;
        private System.Windows.Forms.TrackBar tbrIntervalo;
        private System.Windows.Forms.Label lblTvalor;
        private System.Windows.Forms.ProgressBar pdrTrabajo;
        private System.Windows.Forms.Button btnCalculo;
    }
}

