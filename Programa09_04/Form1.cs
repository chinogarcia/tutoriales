﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programa09_04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Pen pluma1 = new Pen(Color.OrangeRed, 5);
            g.DrawLine(pluma1, 50, 50, 200, 50);

            Pen pluma2 = new Pen(Color.LawnGreen, 7);
            pluma2.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDotDot;
            g.DrawLine(pluma2, 50, 70, 200, 70);

            Pen pluma3 = new Pen(Color.PaleTurquoise, 10);
            float[] guiones = { 1.0f, 1.0f, 2.0f, 1.0f, 3.0f, 1.0f };
            
            pluma3.DashPattern = guiones;
            g.DrawLine(pluma3, 50, 90, 200, 90);

            //Para flechas
            Pen pluma4 = new Pen(Color.DarkKhaki, 15);
            pluma4.StartCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            pluma4.EndCap = System.Drawing.Drawing2D.LineCap.DiamondAnchor;
            g.DrawLine(pluma4, 50, 120, 200, 120);

        }
    }
}
