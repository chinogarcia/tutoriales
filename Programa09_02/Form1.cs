﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programa09_02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            //g.DrawEllipse(Pens.LightGreen, new Rectangle(50, 50, 150, 150));

            //g.TranslateTransform(100, 100);

            //g.DrawEllipse(Pens.Blue, new Rectangle(0, 0, 50, 50));
            //g.DrawEllipse(Pens.DarkGoldenrod, new Rectangle(50, 0, 50, 50));

            //Coordenadas de mundo y pixeles
            g.DrawEllipse(Pens.Red, new Rectangle(0,0, 20,20));

            //Colocamos una nueva ubidad de medida
            //point 1/72 pulgada
            //display 1/100 pulgada de impresoras
            //document 1/300 pulgada

            g.PageUnit = GraphicsUnit.Millimeter;

            g.DrawEllipse(Pens.Bisque, new Rectangle(1, 1, 20, 20));
        }
    }
}
